<div>
    <h1>Ссылка для подтверждения аккаунта</h1>
    <a href="{{getenv('FRONTEND_URL')}}/confirm-email/{{$token}}?email={{$email}}">Подтвердить</a>
</div>
