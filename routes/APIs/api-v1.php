<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'users'], function () {
    Route::post('', 'UserController@store');
    Route::get('{user}', 'UserController@show')->middleware('auth')->middleware('admin');
    Route::patch('details', 'UserController@storeDetails')->middleware('auth');
});

Route::group(['prefix' => 'orders', 'middleware' => ['auth']] , function () {
    Route::get('', 'OrderController@index')->middleware('admin');
    Route::post('', 'OrderController@store');
    Route::get('{orderId}', 'OrderController@show')->middleware('admin');
    Route::put('{order}', 'OrderController@update')->middleware('admin');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::get('me', 'AuthController@me')->middleware('auth');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('logout', 'AuthController@logout')->middleware('auth');
});

Route::group(['prefix' => 'order-statuses', 'middleware' => ['auth', 'admin']], function () {
    Route::get('', 'OrderStatusController@index');
});

Route::group(['prefix' => 'payment-statuses', 'middleware' => ['auth', 'admin']], function () {
    Route::get('', 'PaymentStatusController@index');
});

Route::group(['prefix' => 'reset-password'], function () {
    Route::post('step-one', 'ResetPassword@stepOne');
    Route::post('step-two', 'ResetPassword@stepTwo');
    Route::post('step-three', 'ResetPassword@stepThree');
});

Route::group(['prefix' => 'change-password', 'middleware' => ['auth']], function () {
    Route::post('step-one', 'ChangePassword@stepOne');
    Route::post('step-two', 'ChangePassword@stepTwo');
});

Route::group(['prefix' => 'avatars'], function () {
    Route::post('', 'AvatarController@upload');
});

Route::group(['prefix' => 'user-orders'], function () {
    Route::get('', 'OrderController@findByUser')->middleware('auth');
});

Route::group(['prefix' => 'confirm-email'], function () {
    Route::post('', 'UserController@confirmEmail');
    Route::post('resend', 'UserController@resendConfirmEmail');
});

Route::group(['prefix' => 'verify-phone'], function () {
    Route::post('step-one', 'VerifyPhone@stepOne');
    Route::post('step-two', 'VerifyPhone@stepTwo');
    Route::post('resend', 'VerifyPhone@resend');
});

Route::group(['prefix' => 'payments', 'middleware' => ['auth']], function () {
    Route::post('yandex', 'PaymentController@yandex');
});

Route::group(['prefix' => 'callbacks'], function () {
    Route::post('yandex', 'PaymentCallback@yandex');
});
