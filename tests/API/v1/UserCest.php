<?php

use Illuminate\Support\Str;

class UserCest
{
    public function createWithSuccess(\ApiTester $I)
    {
        $email = Str::random() . '@ya-prod.local';
        $password = '12345678';
        $data = [
            'name'  => 'Foo',
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password
        ];

        $I->sendPost('/api/v1/users', $data);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesXpath('//name');
        $I->seeResponseJsonMatchesXpath('//email');
        $I->seeResponseMatchesJsonType([
            'name'  => 'string',
            'email' => 'string'
        ]);
    }

    public function createWithFailed(\ApiTester $I)
    {
        $invalidData = [
            'name'                  => 'A',
            'email'                 => 'Mock',
            'password'              => 12345678,
            'password_confirmation' => 123
        ];

        $I->sendPost('/api/v1/users', $invalidData);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNPROCESSABLE_ENTITY);
        $I->seeResponseJsonMatchesXpath('//errors');
    }
}
