<?php

use Illuminate\Support\Str;
use Codeception\Util\HttpCode;

class AuthCest
{
    public function authWithSuccess(\ApiTester $I)
    {
        $email = Str::random() . '@ya-prod.local';
        $password = 12345678;
        $credentials = [
            'name'                  => 'Foo',
            'email'                 => $email,
            'password'              => $password,
            'password_confirmation' => $password
        ];
        $I->sendPost('/api/v1/users', $credentials);

        $I->sendPost('/api/v1/auth/login', $credentials);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseJsonMatchesXpath('//access_token');
        $I->seeResponseJsonMatchesXpath('//token_type');
        $I->seeResponseJsonMatchesXpath('//expires_in');
    }

    public function authWithInvalidCredentials(\ApiTester $I)
    {
        $invalidCredentials = [
            'email'     => 'Foo',
            'password'  => 'Foo'
        ];

        $I->sendPost('/api/v1/auth/login', $invalidCredentials);

        $I->seeResponseCodeIs(HttpCode::UNAUTHORIZED);
        $I->seeResponseJsonMatchesXpath('//error');
    }
}
