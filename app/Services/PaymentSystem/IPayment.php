<?php

namespace App\Services\PaymentSystem;

use App\Models\Transaction;

interface IPayment
{
    /**
     * @param Transaction $transaction
     * @param array $options
     * @return mixed
     */
    public function pay(Transaction $transaction, array $options = []);
}
