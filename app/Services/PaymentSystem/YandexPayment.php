<?php


namespace App\Services\PaymentSystem;

use App\Enums\YandexPaymentStatuses;
use App\Models\PaymentStatus;
use Illuminate\Support\Facades\Log;
use YooKassa\Client;
use App\Models\Transaction;
use App\Services\PaymentSystem\IPayment;
use YooKassa\Model\NotificationEventType;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\Notification\NotificationWaitingForCapture;

class YandexPayment implements IPayment
{
    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
        $this->client->setAuth(
            getenv('YANDEX_SHOP_ID'),
            getenv('YANDEX_SECRET_KEY')
        );
    }

    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Transaction $transaction
     * @param array $options
     * @return mixed
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\BadApiRequestException
     * @throws \YooKassa\Common\Exceptions\ForbiddenException
     * @throws \YooKassa\Common\Exceptions\InternalServerError
     * @throws \YooKassa\Common\Exceptions\NotFoundException
     * @throws \YooKassa\Common\Exceptions\ResponseProcessingException
     * @throws \YooKassa\Common\Exceptions\TooManyRequestsException
     * @throws \YooKassa\Common\Exceptions\UnauthorizedException
     */
    public function pay(Transaction $transaction, array $options = [])
    {
        $payment = $this->getClient()->createPayment([
            'amount' => [
                'value'     => $transaction->amount,
                'currency'  => 'RUB'
            ],
            'confirmation' => [
                'type'          => 'redirect',
                'return_url'    => getenv('YANDEX_RETURN_URL')
            ],
            'capture' => false,
            'description' => $transaction->description,
            'metadata' => [
                'transaction_id' => $transaction->id
            ]
        ]);

        return $payment->getConfirmation()->getConfirmationUrl();
    }

    /**
     * @param $requestBody
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\BadApiRequestException
     * @throws \YooKassa\Common\Exceptions\ForbiddenException
     * @throws \YooKassa\Common\Exceptions\InternalServerError
     * @throws \YooKassa\Common\Exceptions\NotFoundException
     * @throws \YooKassa\Common\Exceptions\ResponseProcessingException
     * @throws \YooKassa\Common\Exceptions\TooManyRequestsException
     * @throws \YooKassa\Common\Exceptions\UnauthorizedException
     */
    public function callback($requestBody)
    {
        Log::info($requestBody);

        $notification = ($requestBody['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
            ? new NotificationSucceeded($requestBody)
            : new NotificationWaitingForCapture($requestBody);
        $payment = $notification->getObject();

        if(isset($payment->status) && $payment->status === 'waiting_for_capture')
        {
            $this->getClient()->capturePayment([
                'amount' => $payment->amount
            ], $payment->id, uniqid('', true));
        }

        if(isset($payment->status) && $payment->status === 'succeeded')
        {
            if((bool)$payment->paid === true)
            {
                $metaData = (object)$payment->metadata;
                if($transactionId = $metaData->transaction_id)
                {
                    /* @var Transaction $transaction */
                    $transaction = Transaction::query()->find($transactionId);
                    $transaction->status = YandexPaymentStatuses::CONFIRMED;
                    $transaction->save();

                    $order = $transaction->order;
                    /* @var PaymentStatus $paymentStatus */
                    $paymentStatus = PaymentStatus::findByName(PaymentStatus::PAYED);
                    if($paymentStatus)
                    {
                        $order->payment_status_id = $paymentStatus->id;
                        $order->save();
                    }
                }
            }
        }
    }
}
