<?php


namespace App\Services;

use Exception;
use App\Models\Order;
use App\Models\FormDescriptor;

class OrderService
{
    /**
     * @param string $formKey
     * @param array $data
     * @param int $userId
     * @return mixed
     * @throws Exception
     */
    public static function create($formKey, $data, $userId)
    {
        if(!$formDescriptor = FormDescriptor::findByKey($formKey))
        {
            return null;
        }

        $createdForm = $formDescriptor->model::create($data);
        $createdOrder = Order::create([
            'form_id'           => $createdForm['id'],
            'form_key'          => $formKey,
            'user_id'           => $userId
        ]);

        return $createdOrder;
    }

    /**
     * @param array $orders
     * @param int $userId
     * @throws Exception
     */
    public static function createMany($orders, $userId)
    {
        foreach($orders as $order)
        {
            self::create($order['form_key'], $order, $userId);
        }
    }
}
