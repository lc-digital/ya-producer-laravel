<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangePassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    private $changeToken;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($changeToken)
    {
        $this->changeToken = $changeToken;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Изменение пароля')->
                      view('mails.change-password', ['changeToken' => $this->changeToken]);
    }
}
