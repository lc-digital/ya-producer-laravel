<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuyReadySong extends Model
{
    use HasFactory;

    protected $table = 'buy_ready_song';

    protected $fillable = [
        'song_style',
        'task_content'
    ];
}
