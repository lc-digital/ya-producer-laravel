<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormDescriptor extends Model
{
    use HasFactory;

    protected $fillable = [
        'key',
        'name',
        'model',
        'amount',
        'validations'
    ];

    /**
     * @param string $key
     * @return mixed
     */
    public static function findByKey($key)
    {
        return self::where('key', $key)->first();
    }
}
