<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SongFromScratch extends Model
{
    use HasFactory;

    protected $table = 'songs_from_scratch';

    protected $fillable = [
        'song_style',
        'task_content'
    ];
}
