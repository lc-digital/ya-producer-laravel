<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SongPR extends Model
{
    use HasFactory;

    protected $table = 'songs_pr';

    protected $fillable = [
        'song_link',
        'song_style',
        'song_cover_link',
        'audience',
        'song_description',
        'platforms'
    ];
}
