<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    use HasFactory;

    use HasFactory;

    protected $fillable = [
        'name',
        'is_default'
    ];

    public static function getDefault()
    {
        return self::where('is_default', true)->first();
    }
}
