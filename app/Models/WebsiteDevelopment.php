<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebsiteDevelopment extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_name',
        'song_style',
        'website_tasks',
        'desired_domain',
        'access_to_website',
        'access_to_hosting',
        'website_type',
        'website_structure',
        'with_mobile_version',
        'with_slider',
        'with_search',
        'with_store',
        'with_payment',
        'with_feedback',
        'with_map',
        'website_examples',
        'website_bad_examples',
        'website_competitors_examples',
        'deadline'
    ];
}
