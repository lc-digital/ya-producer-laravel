<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VideoPromotions extends Model
{
    use HasFactory;

    protected $fillable = [
        'video_link',
        'video_style',
        'platforms',
        'audience',
        'video_description'
    ];
}
