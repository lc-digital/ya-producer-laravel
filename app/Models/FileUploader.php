<?php

namespace App\Models;

use Illuminate\Support\Str;

class FileUploader
{
    const NAME_LENGTH = 10;

    public static function upload(array $files, $directory = 'uploads')
    {
        $paths = [];

        foreach($files as $key => $file)
        {
            $fileName = Str::random(self::NAME_LENGTH) . '.' . $file->extension();
            $file->move(public_path($directory), $fileName);
            $paths[$key] = "/$directory/$fileName";
        }

        return $paths;
    }
}
