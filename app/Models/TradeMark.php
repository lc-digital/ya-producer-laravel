<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TradeMark extends Model
{
    use HasFactory;

    protected $fillable = [
        'trade_mark_name',
        'trade_mark_link',
        'trade_mark_owner',
        'ogrn',
        'address',
        'description'
    ];
}
