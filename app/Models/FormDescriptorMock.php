<?php

namespace App\Models;

class FormDescriptorMock
{
    const SONG_FROM_SCRATCH = 'sfs';

    const KEYS = [
        self::SONG_FROM_SCRATCH => [
            'model' => SongFromScratch::class,
            'validations' => [
                'song_style'    => 'required',
                'task_content'  => 'required'
            ]
        ],
        'sft' => [
            'model' => SongFromTemplate::class,
            'validations' => [
                'song_style'        => 'required',
                'task_content'      => 'required'
            ]
        ],
        'brsf' => [
            'model' => BuyReadySong::class,
            'validations' => [
                'song_style'        => 'required',
                'task_content'      => 'required'
            ]
        ],
        'sc' => [
            'model' => SongCover::class,
            'validations' => [
                'project_name'      => 'required',
                'song_style'        => 'required',
                'task_content'      => 'required'
            ]
        ],
        'rs' => [
            'model' => ReleaseSong::class,
            'validations' => [
                'artist'        => 'required',
                'song_name'     => 'required',
                'song_style'    => 'required',
                'song_author'   => 'required',
                'words_author'  => 'required',
                'language'      => 'required',
                'lyric'         => 'required'
            ]
        ],
        'sp' => [
            'model' => SongPR::class,
            'validations' => [
                'song_link'         => 'required',
                'song_style'        => 'required',
                'song_cover_link'   => 'required',
                'audience'          => 'required',
                'song_description'  => 'required',
                'platforms'         => 'required'
            ]
        ],
        'lv' => [
            'model' => LyricVideo::class,
            'validations' => [
                'song_link'     => 'required',
                'task_content'  => 'required'
            ]
        ],
        'mv' => [
            'model' => MusicVideo::class,
            'validations' => [
                'song_link'     => 'required',
                'task_content'  => 'required'
            ]
        ],
        'vp' => [
            'model' => VideoPromotions::class,
            'validations' => [
                'video_link'        => 'required',
                'video_style'       => 'required',
                'platforms'         => 'required',
                'audience'          => 'required',
                'video_description' => 'required'
            ]
        ],
        'an' => [
            'model' => ArtistName::class,
            'validations' => [
                'task_content' => 'required'
            ]
        ],
        'tm' => [
           'model' => TradeMark::class,
           'validations' => [
               'trade_mark_name'    => 'required',
               'trade_mark_link'    => 'required',
               'trade_mark_owner'   => 'required',
               'ogrn'               => 'required',
               'address'            => 'required',
               'description'        => 'required'
           ]
        ],
        'd' => [
            'model' => Domain::class,
            'validations' => [
                'name'                          => 'required',
                'fill_name_trade_mark_owner'    => 'required',
                'with_hosting'                  => 'required',
                'description'                   => 'required'
            ]
        ],
        'p' => [
            'model' => Platform::class,
            'validations' => [
                'project_name'  => 'required',
                'platforms'     => 'required',
                'description'   => 'required'
            ]
        ],
        'l' => [
            'model' => Logo::class,
            'validations' => [
                'song_style'    => 'required',
                'task_content'  => 'required'
            ]
        ],
        'bb' => [
            'model' => Brandbook::class,
            'validations' => [
                'song_style'    => 'required',
                'task_content'  => 'required'
            ]
        ],
        'song_cover' => [
            'model' => SingleCover::class,
            'validations' => [
                'name'          => 'required',
                'song_style'    => 'required',
                'cover_link'    => 'required',
                'task_content'  => 'required'
            ]
        ],
        'smd' => [
            'model' => SocialMediaDesign::class,
            'validations' => [
                'project_name'  => 'required',
                'song_style'    => 'required',
                'platforms'     => 'required',
                'task_content'  => 'required'
            ]
        ],
        'wd' => [
            'model' => WebsiteDevelopment::class,
            'validations' => [
                'project_name'                  => 'required',
                'song_style'                    => 'required',
                'website_tasks'                 => 'required',
                'desired_domain'                => 'required',
                'access_to_website'             => 'required',
                'access_to_hosting'             => 'required',
                'website_type'                  => 'required',
                'website_structure'             => 'required',
                'with_mobile_version'           => 'required',
                'with_slider'                   => 'required',
                'with_search'                   => 'required',
                'with_store'                    => 'required',
                'with_payment'                  => 'required',
                'with_feedback'                 => 'required',
                'with_map'                      => 'required',
                'website_examples'              => 'required',
                'website_bad_examples'          => 'required',
                'website_competitors_examples'  => 'required',
                'deadline'                      => 'required'
            ]
        ],
        'v' => [
            'model' => Vote::class,
            'validations' => [
                'project_name' => 'required',
                'vote_link'    => 'required',
                'task_content' => 'required'
            ]
        ],
        'wiki' => [
            'model' => Wikipedia::class,
            'validations' => [
                'project_name'  => 'required',
                'biography'     => 'required'
            ]
        ],
        'pr' => [
            'model' => PressRelease::class,
            'validations' => [
                'project_name' => 'required',
                'news_title'   => 'required',
                'task_content' => 'required'
            ]
        ],
        'm' => [
            'model' => Media::class,
            'validations' => [
                'project_name'          => 'required',
                'title_news'            => 'required',
                'illustration_link'     => 'required',
                'press_release'         => 'required',
                'task_content'          => 'required'
            ]
        ],
        'pm' => [
            'model' => ReputationMonitoring::class,
            'validations' => [
                'project_name'          => 'required',
                'project_description'   => 'required'
            ]
        ],
        'rn' => [
            'model' => RemoveNegative::class,
            'validations' => [
                'project_name'      => 'required',
                'negative_link'     => 'required',
                'problem'           => 'required'
            ]
        ]
    ];

    public static function get($key)
    {
        return self::KEYS[$key];
    }

    public static function getModel($key)
    {
        return self::get($key)['model'];
    }

    public static function getKeys()
    {
        return array_keys(self::KEYS);
    }
}
