<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    use HasFactory;

    const DEFAULT   = 0;
    const MALE      = 1;
    const FEMALE    = 2;
    const OTHER     = 3;

    protected $fillable = [
        'gender',
        'birth_day',
        'birth_month',
        'birth_year'
    ];

    public function getGenderName($index)
    {
         static $genderNames = [
            self::MALE      => 'Мужчина',
            self::FEMALE    => 'Женищна',
            self::OTHER     => 'Другой',
            self::DEFAULT   => 'Не указан'
         ];

         return $genderNames[$index] ?? $genderNames[self::DEFAULT];
    }

    /**
     * Convert a number of gender to gender name
     *
     * @param $value
     * @return string
     */
    public function getGenderAttribute($value)
    {
        return $this->getGenderName($value);
    }
}
