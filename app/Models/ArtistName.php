<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArtistName extends Model
{
    use HasFactory;

    protected $table = 'artists_name';

    protected $fillable = [
        'task_content'
    ];
}
