<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RemoveNegative extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_name',
        'negative_link',
        'problem'
    ];
}
