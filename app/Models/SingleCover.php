<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SingleCover extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'song_style',
        'cover_link',
        'task_content'
    ];
}
