<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SongFromTemplate extends Model
{
    use HasFactory;

    protected $table = 'songs_from_template';

    protected $fillable = [
        'song_style',
        'song_template_src',
        'task_content'
    ];
}
