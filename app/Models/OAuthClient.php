<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OAuthClient extends Model
{
    use HasFactory;

    protected $table = 'oauth_clients';

    protected $fillable = [
        'name',
        'description',
        'application_token'
    ];

    public static function findByApplicationToken($applicationToken)
    {
        return self::where('application_token', $applicationToken)->firstOrFail();
    }
}
