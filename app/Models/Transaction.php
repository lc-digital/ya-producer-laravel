<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $amount
 * @property int $user_id
 * @property int $order_id
 * @property string $description
 * @property int $status
 * @property Order $order
 */
class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'order_id', 'description', 'status', 'amount'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @param User $user
     * @param Order $order
     * @return mixed
     */
    public static function create(User $user, Order $order)
    {
        $created = (new static)->newQuery()->create([
            'user_id'   => $user->id,
            'order_id'  => $order->id,
            'amount'    => (int)$order->formDescriptor()->amount / 100
        ]);

        return self::query()->find($created['id']);
    }
}
