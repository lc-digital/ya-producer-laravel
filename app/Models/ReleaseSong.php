<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReleaseSong extends Model
{
    use HasFactory;

    protected $fillable = [
        'song_src',
        'artist',
        'song_name',
        'cover_src',
        'song_style',
        'song_author',
        'words_author',
        'language',
        'lyric'
    ];
}
