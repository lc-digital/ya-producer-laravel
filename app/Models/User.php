<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property int $id
 * @property string $phone
 * @property string $password
 * @property string $email_verify_token
 * @property object $userDetails
 * @property bool $remember_user
 * @property ResetPasswordToken $resetPasswordToken
 * @property string $phone_verify_code
 * @property int $phone_verified_at
 */
class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_details_id',
        'avatar',
        'name',
        'surname',
        'midname',
        'email',
        'email_verified_at',
        'phone',
        'phone_verified_at',
        'password',
        'remember_user',
        'password_reset_token',
        'change_password_token',
        'email_verify_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'password_reset_token',
        'change_password_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rules()
    {
        return $this->belongsToMany(Rule::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function userDetails()
    {
        return $this->belongsTo(UserDetail::class);
    }

    public function resetPasswordToken()
    {
        return $this->belongsTo(ResetPasswordToken::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @param bool
     * @return void
     */
    public function rememberMe($value)
    {
        $this->remember_user = $value;
        $this->save();
    }

    /**
     * @param string $email
     * */
    public static function findByEmail($email)
    {
        return self::where('email', $email)->first();
    }

    public static function create(array $attribute)
    {
        $attribute['email_verify_token'] = Str::random();

        return (new static)->newQuery()->create($attribute);
    }
}
