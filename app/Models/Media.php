<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_name',
        'title_news',
        'illustration_link',
        'press_release',
        'task_content'
    ];
}
