<?php

namespace App\Models;

use App\Models\FormDescriptor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property $form
 * @property $form_id
 * @property $form_key
 * @property $form_descriptor
 * @property int $payment_status_id
 */
class Order extends Model
{
    use HasFactory;

    const LIMIT = 10;

    protected $fillable = [
        'form_id',
        'form_key',
        'user_id',
        'order_status_id',
        'payment_status_id',
        'note'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function formDescriptor()
    {
        return FormDescriptor::findByKey($this->form_key);
    }

    public function paymentStatus()
    {
        return $this->belongsTo(PaymentStatus::class);
    }

    public function getForm()
    {
        $this->form_descriptor = FormDescriptor::findByKey($this->form_key);
        $this->form = $this->form_descriptor->model::find($this->form_id);
    }

    public static function create(array $attributes)
    {
        $attributes['order_status_id'] = OrderStatus::getDefault()->id;
        $attributes['payment_status_id'] = PaymentStatus::getDefault()->id;

        return (new static)->newQuery()->create($attributes);
    }
}
