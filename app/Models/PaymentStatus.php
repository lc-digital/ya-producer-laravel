<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 */
class PaymentStatus extends Model
{
    use HasFactory;

    const PAYED = 'Оплачен';

    protected $fillable = [
        'name',
        'is_default'
    ];

    public static function getDefault()
    {
        return self::where('is_default', true)->first();
    }

    /**
     * @param $name
     * @return \Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    public static function findByName($name)
    {
        return self::query()->where('name', $name)->first();
    }
}
