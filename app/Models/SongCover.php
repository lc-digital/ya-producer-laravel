<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SongCover extends Model
{
    use HasFactory;

    protected $table = 'songs_cover';

    protected $fillable = [
        'project_name',
        'song_style',
        'file_src',
        'task_content'
    ];
}
