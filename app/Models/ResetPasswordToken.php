<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property string $token
 * @property int $expired_at
 */
class ResetPasswordToken extends Model
{
    use HasFactory;

    const EXPIRED_TIME = 10 * 60;

    protected $fillable = [
        'token',
        'expired_at'
    ];

    private static function getExpiredTime()
    {
        return getenv('RESET_TOKEN_EXPIRED_TIME', self::EXPIRED_TIME);
    }

    public static function create()
    {
        return (new static)->newQuery()->create([
            'token'         => Str::random(),
            'expired_at'    => now()->addSeconds(self::getExpiredTime())
        ]);
    }
}
