<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialMediaDesign extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_name',
        'song_style',
        'platforms',
        'task_content'
    ];
}
