<?php

namespace App\Console\Commands;

use Exception;
use PHPHtmlParser\Dom;
use App\Models\FormDescriptor;
use Illuminate\Console\Command;
use PHPHtmlParser\Exceptions\NotLoadedException;
use PHPHtmlParser\Exceptions\ChildNotFoundException;

class Former extends Command
{
    /**
     * @var string
     */
    private $targetTag = 'descriptor';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'former:xml {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse a xml file with form descriptors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws NotLoadedException
     * @throws ChildNotFoundException
     * @throws \PHPHtmlParser\Exceptions\CircularException
     */
    public function handle()
    {
        $xml = new Dom();
        $path = $this->argument('path');

        try
        {
            $xml->loadFromFile($path);
        }
        catch(Exception $exception)
        {
            $this->error("File with path $path not found");
            return 1;
        }

        $descriptors = $xml->find($this->targetTag);
        /* @var Dom\Node\AbstractNode $descriptor */
        foreach ($descriptors as $descriptor)
        {
            $validations = [];

            $formKey = $descriptor->getAttribute('key');
            $name = $descriptor->getAttribute('name');
            $model = $descriptor->find('model')->text;
            $amountContainer = $descriptor->find('amount');
            $validationsContainer = $descriptor->find('validations');
            $amount = $amountContainer->count() ? $amountContainer->text : null;

            /* @var Dom\Node\AbstractNode $validation */
            foreach($validationsContainer->getChildren() as $validation)
            {
                if($validation->tag->name() !== 'text')
                {
                    $rules = $validation->getAttributes();
                    $encodedRules = '';

                    foreach($rules as $key => $value)
                    {
                        if($value === 'true')
                        {
                            $encodedRules .= "$key|";
                        }
                        else
                        {
                            $encodedRules .= "$key:$value|";
                        }
                    }

                    $encodedRules = substr($encodedRules, 0, -1);
                    $validations[$validation->tag->name()] = $encodedRules;
                }
            }

            $formDescriptor = [
                'key'           => $formKey,
                'name'          => $name,
                'model'         => "App\\Models\\$model",
                'amount'        => $amount,
                'validations'   => json_encode($validations)
            ];

            FormDescriptor::query()->create($formDescriptor);

            $this->info("Created form descriptor with key $formKey");
        }

        return 0;
    }
}
