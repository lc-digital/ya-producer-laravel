<?php

namespace App\Factories;

use stdClass;

class SmsFactory
{
    /**
     * @param $text
     * @param $phone
     * @return stdClass
     */
    public static function factory($text, $phone)
    {
        $sms = new stdClass();

        $sms->to = $phone;
        $sms->text= $text;

        return $sms;
    }

    /**
     * @param $code
     * @param $phone
     * @return stdClass
     */
    public static function factoryVerifyMessage($code, $phone)
    {
        return self::factory("Ваш код подтверждения: $code", $phone);
    }
}
