<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /* @var User $user */
        $user = User::find(auth()->id());
        foreach($user->rules as $rule)
        {
            if($rule->name === 'admin')
            {
                return $next($request);
            }
        }

        return response()->json([
            'message' => 'Permission denied'
        ], 401);
    }
}
