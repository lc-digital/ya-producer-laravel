<?php

namespace App\Http\Controllers\API\v1;

use App\Models\Order;
use App\Mail\CreatedOrder;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\CreateOrderRequest;

class OrderController extends Controller
{
    #Route::get('/api/v1/orders');
    public function index()
    {
        $orders = Order::with(['user', 'orderStatus', 'paymentStatus'])->
                         orderBy('id', 'DESC')->
                         paginate(Order::LIMIT);

        foreach($orders as $order)
        {
            $order->getForm();
        }

        return $orders;
    }

    #Route::get('/api/v1/user-orders')
    public function findByUser()
    {
        $orders = Order::with(['orderStatus', 'paymentStatus'])->
                         where('user_id', auth()->id())->
                         orderBy('id', 'DESC')->
                         paginate(Order::LIMIT);

        foreach($orders as $order)
        {
            $order->getForm();
        }

        return $orders;
    }

    #Route::post('/api/v1/orders');
    public function store(CreateOrderRequest $request)
    {
        $createdOrder = OrderService::create(
            $request->input('form_key'),
            $request->input(),
            auth()->id()
        );

        Mail::to(auth()->user())->queue(new CreatedOrder($createdOrder));

        return $createdOrder;
    }

    #Route::post('/api/v1/orders/{orderId}')
    public function show($orderId)
    {
        $order = Order::with(['user', 'orderStatus', 'paymentStatus'])->
                        findOrFail($orderId);
        $order->getForm();

        return $order;
    }

    #Route::put('/api/v1/orders/{order}')
    public function update(Order $order, Request $request)
    {
        $order->update($request->input());

        return $order;
    }
}
