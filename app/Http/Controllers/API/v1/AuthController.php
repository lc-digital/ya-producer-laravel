<?php

namespace App\Http\Controllers\API\v1;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    #Route::post('/api/v1/auth/login');
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if(!$token = auth()->attempt($credentials))
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        /* @var User $user */
        $currentUser = User::find(auth()->id());
        $remember = $request->exists('remember_user') ? $request->boolean('remember_user') : false;
        $currentUser->rememberMe($remember);

        return $this->respondWithToken($token);
    }

    #Route::get('/api/v1/auth/me');
    public function me()
    {
        return response()->json(auth()->user());
    }

    #Route::get('/api/v1/auth/refresh');
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    #Route::post('/api/v1/auth/logout');
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
}
