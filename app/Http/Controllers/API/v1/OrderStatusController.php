<?php

namespace App\Http\Controllers\API\v1;

use App\Models\OrderStatus;
use App\Http\Controllers\Controller;

class OrderStatusController extends Controller
{
    public function index()
    {
        return OrderStatus::get();
    }
}
