<?php

namespace App\Http\Controllers\API\v1;

use App\Models\User;
use App\Helpers\TokenGenerator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordStepTwo;
use App\Mail\ChangePassword as MailChangePassword;

class ChangePassword extends Controller
{
    #Route::post('/api/v1/change-password/step-one')
    public function stepOne()
    {
        /* @var User $user */
        $user = User::find(auth()->id());
        $user->change_password_token = TokenGenerator::numberToken();
        $user->save();

        Mail::to($user)->queue(new MailChangePassword($user->change_password_token));

        return response()->json(['message' => 'Token generated']);
    }

    #Route::post('/api/v1/change-password/step-two')
    public function stepTwo(ChangePasswordStepTwo $request)
    {
        /* @var User $user */
        $user = User::find(auth()->id());
        $passwordChangeToken = $request->input('password_change_token');

        if($user->change_password_token != $passwordChangeToken)
        {
            return response()->json([
                'message' => 'Permission denied'
            ], 401);
        }

        $user->password = Hash::make($request->input('password'));
        $user->change_password_token = null;

        return response()->json(['message' => 'Password changed']);
    }
}
