<?php

namespace App\Http\Controllers\API\v1;

use App\Models\PaymentStatus;
use App\Http\Controllers\Controller;

class PaymentStatusController extends Controller
{
    public function index()
    {
        return PaymentStatus::get();
    }
}
