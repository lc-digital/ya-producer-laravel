<?php

namespace App\Http\Controllers\API\v1;

use App\Models\User;
use App\Helpers\SmsClient;
use App\Factories\SmsFactory;
use Illuminate\Support\Carbon;
use App\Helpers\TokenGenerator;
use App\Http\Controllers\Controller;
use App\Http\Requests\VerifyPhone\StepOne;
use App\Http\Requests\VerifyPhone\StepTwo;

class VerifyPhone extends Controller
{
    public function stepOne(StepOne $request)
    {
        /* @var User $user */
        $user = User::find(auth()->id());

        $user->phone = $request->input('phone');
        $user->phone_verify_code = TokenGenerator::numberToken();
        $user->save();

        $sms = SmsFactory::factoryVerifyMessage($user->phone_verify_code, $user->phone);
        SmsClient::send($sms);

        return response()->json(['message' => 'Code generated']);
    }

    public function stepTwo(StepTwo $request)
    {
        /* @var User $user */
        $user = User::find(auth()->id());

        if($user->phone_verify_code !== $request->input('code'))
        {
            return response()->json([
                'message' => 'Invalid code'
            ], 401);
        }

        $user->phone_verified_at = Carbon::now();
        $user->save();

        return response()->json(['message' => 'Phone verified']);
    }

    public function resend()
    {
        /* @var User $user */
        $user = User::find(auth()->id());

        if($user->phone === null) {
            return response()->json([
               'message' => 'Phone not exists'
            ], 401);
        }

        $user->phone_verify_code = TokenGenerator::numberToken();
        $sms = SmsFactory::factoryVerifyMessage($user->phone_verify_code, $user->phone);
        SmsClient::send($sms);

        return response()->json(['message' => 'Code generated']);
    }
}
