<?php

namespace App\Http\Controllers\API\v1;

use App\Models\User;
use App\Models\Order;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PaymentSystem\YandexPayment;

class PaymentController extends Controller
{
    #Route::post('/api/v1/payments/yandex')
    public function yandex(Request $request)
    {
        $yandexPayment = new YandexPayment();
        /* @var User $user */
        $user = User::query()->find(auth()->id());
        /* @var Order $order */
        $order = $user->orders()->findOrFail((int)$request->input('order_id'));
        /* @var Transaction $transaction */
        $transaction = Transaction::create($user, $order);

        return $yandexPayment->pay($transaction);
    }
}
