<?php

namespace App\Http\Controllers\API\v1;

use Exception;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Models\ResetPasswordToken;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Helpers\ResetPasswordTokenChecker;
use App\Mail\ResetPassword as MailResetPassword;
use App\Http\Requests\ResetPasswordStepOneRequest;
use App\Http\Requests\ResetPasswordStepTwoRequest;
use App\Http\Requests\ResetPasswordStepThreeRequest;

class ResetPassword extends Controller
{
    #Route::post('/api/v1/reset-password/step-one')
    public function stepOne(ResetPasswordStepOneRequest $request)
    {
        /* @var User $user */
        $user = User::findByEmail($request->input('email'));

        DB::transaction(function () use ($user) {
            if($user->resetPasswordToken)
            {
                $user->resetPasswordToken()->delete();
            }
            $user->resetPasswordToken()->associate(ResetPasswordToken::create());
            $user->save();
        });

        Mail::to($user)->queue(new MailResetPassword($user->resetPasswordToken->token));

        return response()->json(['message' => 'Token generated']);
    }

    #Route::post('/api/v1/reset-password/step-two')
    public function stepTwo(ResetPasswordStepTwoRequest $request)
    {
        /* @var User $user */
        $user = User::findByEmail($request->input('email'));
        $resetPasswordToken = $request->input('reset_password_token');

        try
        {
            (new ResetPasswordTokenChecker($user))->check($resetPasswordToken);
        } catch(Exception $exception)
        {
            return response()->json([
                'message' => $exception->getMessage()
            ], 401);
        }

        return response()->json(['message' => 'Token checked']);
    }

    #Route::post('/api/v1/reset-password/step-three')
    public function stepThree(ResetPasswordStepThreeRequest $request)
    {
        /* @var User $user */
        $user = User::findByEmail($request->input('email'));
        $resetPasswordToken = $request->input('reset_password_token');

        try
        {
            (new ResetPasswordTokenChecker($user))->check($resetPasswordToken);
        } catch(Exception $exception)
        {
            return response()->json([
                'message' => $exception->getMessage()
            ], 401);
        }

        DB::transaction(function () use ($user, $request) {
            $user->resetPasswordToken()->delete();
            $user->password = Hash::make($request->input('password'));
            $user->save();
        });

        return response()->json(['message' => 'Password changed']);
    }
}
