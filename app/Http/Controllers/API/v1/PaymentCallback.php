<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PaymentSystem\YandexPayment;

class PaymentCallback extends Controller
{
    public function yandex()
    {
        $yandexPayment = new YandexPayment();
        $source = file_get_contents('php://input');
        $requestBody = json_decode($source, true);

        $yandexPayment->callback($requestBody);
    }
}
