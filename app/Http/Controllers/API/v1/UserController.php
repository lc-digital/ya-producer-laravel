<?php

namespace App\Http\Controllers\API\v1;

use App\Models\User;
use App\Mail\Welcome;
use App\Mail\ConfirmMail;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Mail\UpdatedUserDetails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\CreateUserDetailsRequest;
use Illuminate\Support\Str;

class UserController extends Controller
{
    #Route::post('/api/v1/users');
    public function store(CreateUserRequest $request)
    {
        $request->merge(['password' => Hash::make($request->input('password'))]);
        $createdUser = User::create($request->input());

        Mail::to($createdUser)->queue(new Welcome($createdUser));
        Mail::to($createdUser)->queue(new ConfirmMail($createdUser));

        if($importableOrders = $request->input('orders'))
        {
            OrderService::createMany($importableOrders, $createdUser->id);
        }

        return $createdUser;
    }

    #Route::patch('/api/v1/users/details');
    public function storeDetails(CreateUserDetailsRequest $request)
    {
        /* @var User $user */
        $user = auth()->user();
        $userDetails = UserDetail::create($request->input());

        $user->userDetails()->dissociate();
        $user->userDetails()->associate($userDetails);

        $user->save();

        Mail::to($user)->queue(new UpdatedUserDetails());

        return $user;
    }

    #Route::get('/api/v1/users/{user}')
    public function show(User $user)
    {
        $user->rules;

        return $user;
    }

    #Route::post('/api/v1/confirm-email')
    public function confirmEmail(Request $request)
    {
        /* @var User $user */
        $user = User::findByEmail($request->input('email'));

        if($user->hasVerifiedEmail())
        {
            return response()->json([
                'message' => 'Email already verified'
            ], 401);
        }

        if($user->email_verify_token !== $request->input('verify_token'))
        {
            return response()->json([
                'message' => 'Invalid token'
            ], 401);
        }

        $user->markEmailAsVerified();
        $user->email_verify_token = null;
        $user->save();

        return response()->json([
            'message' => 'Email verified'
        ]);
    }

    #Route::post('/api/v1/confirm-email/resend')
    public function resendConfirmEmail(Request $request)
    {
        /* @var User $user */
        $user = User::findByEmail($request->input('email'));
        if($user->hasVerifiedEmail())
        {
            return response()->json([
                'message' => 'Email already verified'
            ], 401);
        }

        $user->email_verify_token = Str::random();
        $user->save();

        Mail::to($user)->queue(new ConfirmMail($user));

        return response()->json([
            'message' => 'resend'
        ]);
    }
}
