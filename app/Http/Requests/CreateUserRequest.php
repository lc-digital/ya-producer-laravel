<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    public function messages()
    {
        return [
            'name.required'     => 'Заполните поле имя',
            'name.min'          => 'Минимальный размер имени - 2 символа',
            'email.required'    => 'Заполните поле почты',
            'email.email'       => 'Некорректный формат почты',
            'email.unique'      => 'Почта уже занята',
            'password.required' => 'Заполните поле пароля',
            'password.min'      => 'Минимальный размер пароля - 8 символов'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|min:2',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|min:8'
        ];
    }
}
