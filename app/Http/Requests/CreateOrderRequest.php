<?php

namespace App\Http\Requests;

use App\Models\FormDescriptor;
use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'form_key' => 'required|exists:form_descriptors,key'
        ];

        $formKey = $this->input('form_key');
        if($descriptor = FormDescriptor::findByKey($formKey))
        {
            $rules = array_merge($rules, json_decode($descriptor->validations, true));
        }

        return $rules;

    }
}
