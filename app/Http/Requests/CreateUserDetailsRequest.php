<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserDetailsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'birth_day'     => 'numeric',
            'birth_month'   => 'numeric',
            'birth_year'    => 'numeric'
        ];
    }
}
