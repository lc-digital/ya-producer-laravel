<?php

namespace App\Http\Requests\VerifyPhone;

use Illuminate\Foundation\Http\FormRequest;

class StepTwo extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required'
        ];
    }
}
