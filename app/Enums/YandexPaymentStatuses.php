<?php


namespace App\Enums;


class YandexPaymentStatuses
{
    public const CREATED = 'CREATED';
    public const FAILED = 'FAILED';
    public const CONFIRMED = 'CONFIRMED';
}
