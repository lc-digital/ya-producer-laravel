<?php


namespace App\Helpers;

use Exception;
use Carbon\Carbon;
use App\Models\User;

class ResetPasswordTokenChecker
{
    /**
     * @var User
     */
    private $targetUser;

    public function __construct(User $user)
    {
        $this->targetUser = $user;
    }

    /**
     * @param $resetPasswordToken
     * @throws Exception
     */
    public function check($resetPasswordToken)
    {
        if($this->targetUser === null || $this->targetUser->resetPasswordToken === null)
        {
            throw new Exception('Bad request');
        }
        if($this->targetUser->resetPasswordToken->token !== $resetPasswordToken)
        {
            throw new Exception('Invalid token');
        }
        if(now()->timestamp > Carbon::create($this->targetUser->resetPasswordToken->expired_at)->timestamp)
        {
            throw new Exception('Token expired');
        }
    }
}
