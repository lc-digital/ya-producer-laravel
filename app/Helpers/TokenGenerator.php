<?php

namespace App\Helpers;

class TokenGenerator
{
    public static function numberToken()
    {
        return random_int(1000, 9999);
    }
}
