<?php

namespace App\Helpers;

use \stdClass;
use App\Services\SMSRU;

class SmsClient
{
    public static function send(stdClass $sms)
    {
        $client = new SMSRU(getenv('SMSRU'));
        $client->send_one($sms);
    }
}
