<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsiteDevelopmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_developments', function (Blueprint $table) {
            $table->id();
            $table->string('project_name');
            $table->string('song_style');
            $table->text('website_tasks');
            $table->text('desired_domain');
            $table->string('access_to_website');
            $table->string('access_to_hosting');
            $table->string('website_type');
            $table->text('website_structure');
            $table->integer('with_mobile_version')->default(0);
            $table->integer('with_slider')->default(0);
            $table->integer('with_search')->default(0);
            $table->integer('with_store')->default(0);
            $table->integer('with_payment')->default(0);
            $table->integer('with_feedback')->default(0);
            $table->integer('with_map')->default(0);
            $table->text('website_examples');
            $table->text('website_bad_examples');
            $table->text('website_competitors_examples');
            $table->string('deadline');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_developments');
    }
}
