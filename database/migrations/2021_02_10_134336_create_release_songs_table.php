<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReleaseSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('release_songs', function (Blueprint $table) {
            $table->id();
            $table->string('song_src');
            $table->string('artist');
            $table->string('song_name');
            $table->string('cover_src');
            $table->string('song_style');
            $table->string('song_author');
            $table->string('words_author');
            $table->string('language');
            $table->text('lyric');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('release_song');
    }
}
