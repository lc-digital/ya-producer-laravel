<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_details_id')->references('id')->on('user_details')->nullable();
            $table->string('avatar')->default('uploads/avatars/default.png');
            $table->string('name');
            $table->string('surname')->nullable();
            $table->string('midname')->nullable();

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('email_verify_token')->nullable();

            $table->string('phone')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->string('phone_verify_code')->nullable();

            $table->string('password');
            $table->integer('reset_password_token_id')->nullable();
            $table->string('change_password_token')->nullable();
            $table->boolean('remember_user')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
