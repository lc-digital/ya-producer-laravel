<?php

namespace Database\Seeders;

use App\Models\Rule;
use App\Models\OrderStatus;
use App\Models\PaymentStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    public function createOrderStatuses()
    {
        $statuses = [
            ['name' => 'В процессе', 'is_default' => true],
            ['name' => 'Выполнен'],
            ['name' => 'Отмене']
        ];

        foreach ($statuses as $status)
        {
            OrderStatus::create($status);
        }
    }

    public function createPaymentStatuses()
    {
        $statuses = [
            ['name' => 'Ожидание оплаты', 'is_default' => true],
            ['name' => 'Оплачен'],
            ['name' => 'Отменен возврат']
        ];

        foreach($statuses as $status)
        {
            PaymentStatus::create($status);
        }
    }

    public function createRules()
    {
        Rule::create(['name' => 'admin']);
    }

    public function createFormDescriptors()
    {
        DB::insert("INSERT INTO `form_descriptors` (`id`, `key`, `name`, `model`, `validations`, `created_at`, `updated_at`) VALUES
        (1, 'sfs', 'Песня с нуля', 'App\\Models\\SongFromScratch', '{\"song_style\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (2, 'sft', 'Песня по шаблону', 'App\\Models\\SongFromTemplate', '{\"song_style\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (3, 'brsf', 'Купить готовую песню', 'App\\Models\\BuyReadySong', '{\"song_style\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (4, 'sc', 'Создание обложки песни', 'App\\Models\\SongCover', '{\"project_name\":\"required\",\"song_style\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (5, 'rs', 'Выпуск (релиз) песни', 'App\\Models\\ReleaseSong', '{\"artist\":\"required\",\"song_name\":\"required\",\"song_style\":\"required\",\"song_author\":\"required\",\"words_author\":\"required\",\"language\":\"required\",\"lyric\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (6, 'sp', 'Продвижение песни', 'App\\Models\\SongPR', '{\"song_link\":\"required\",\"song_style\":\"required\",\"song_cover_link\":\"required\",\"audience\":\"required\",\"song_description\":\"required\",\"platforms\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (7, 'lv', 'Создание Lyric Video', 'App\\Models\\LyricVideo', '{\"song_link\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (8, 'mv', 'Создание клипа', 'App\\Models\\MusicVideo', '{\"song_link\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (9, 'vp', 'Продвижение клипа', 'App\\Models\\VideoPromotions', '{\"video_link\":\"required\",\"video_style\":\"required\",\"platforms\":\"required\",\"audience\":\"required\",\"video_description\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (10, 'an', 'Создание псевдонима для артиста', 'App\\Models\\ArtistName', '{\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (11, 'tm', 'Создание торговой марки', 'App\\Models\\TradeMark', '{\"trade_mark_name\":\"required\",\"trade_mark_link\":\"required\",\"trade_mark_owner\":\"required\",\"ogrn\":\"required\",\"address\":\"required\",\"description\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (12, 'd', 'Создание домена', 'App\\Models\\Domain', '{\"name\":\"required\",\"fill_name_trade_mark_owner\":\"required\",\"with_hosting\":\"required\",\"description\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (13, 'p', 'Продвижение платформ', 'App\\Models\\Platform', '{\"project_name\":\"required\",\"platforms\":\"required\",\"description\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (14, 'l', 'Создание логотипа', 'App\\Models\\Logo', '{\"song_style\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (15, 'bb', 'Создание бренд бука', 'App\\Models\\Brandbook', '{\"song_style\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (16, 'song_cover', 'Обложка для сингла', 'App\\Models\\SingleCover', '{\"name\":\"required\",\"song_style\":\"required\",\"cover_link\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (17, 'smd', 'Дизайн социальных сетей', 'App\\Models\\SocialMediaDesign', '{\"project_name\":\"required\",\"song_style\":\"required\",\"platforms\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (18, 'wd', 'Создание веб сайта', 'App\\Models\\WebsiteDevelopment', '{\"project_name\":\"required\",\"song_style\":\"required\",\"website_tasks\":\"required\",\"desired_domain\":\"required\",\"access_to_website\":\"required\",\"access_to_hosting\":\"required\",\"website_type\":\"required\",\"website_structure\":\"required\",\"with_mobile_version\":\"required\",\"with_slider\":\"required\",\"with_search\":\"required\",\"with_store\":\"required\",\"with_payment\":\"required\",\"with_feedback\":\"required\",\"with_map\":\"required\",\"website_examples\":\"required\",\"website_bad_examples\":\"required\",\"website_competitors_examples\":\"required\",\"deadline\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (19, 'v', 'Голосование', 'App\\Models\\Vote', '{\"project_name\":\"required\",\"vote_link\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (20, 'wiki', 'Википедия', 'App\\Models\\Wikipedia', '{\"project_name\":\"required\",\"biography\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (21, 'pr', 'Пресс релиз', 'App\\Models\\PressRelease', '{\"project_name\":\"required\",\"news_title\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (22, 'm', 'Медиа', 'App\\Models\\Media', '{\"project_name\":\"required\",\"title_news\":\"required\",\"illustration_link\":\"required\",\"press_release\":\"required\",\"task_content\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (23, 'pm', 'Мониторинг репутации', 'App\\Models\\ReputationMonitoring', '{\"project_name\":\"required\",\"project_description\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21'),
        (24, 'rn', 'Удаление негатива', 'App\\Models\\RemoveNegative', '{\"project_name\":\"required\",\"negative_link\":\"required\",\"problem\":\"required\"}', '2021-03-14 20:46:21', '2021-03-14 20:46:21');
        ");
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->createRules();
        $this->createOrderStatuses();
        $this->createPaymentStatuses();
        $this->createFormDescriptors();
    }
}
